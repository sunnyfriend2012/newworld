import React from 'react'
import { useRouter } from 'next/router'
export default function contact() {
    
  const router = useRouter()
    return (
        <>
         <ul>
        <li>
       
          <a className="active"  onClick={() => router.push('/')}>
            Home
          </a>
        
        </li>
        <li>
      <a  onClick={() => router.push('/contact')}>Contact</a>
        </li>
        <li>
        <a  onClick={() => router.push('/about')}>About</a>
        </li>
      </ul>
        </>
    )
}
